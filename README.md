Git global setup

git config --global user.name "Anirudh Menon"
git config --global user.email "anirudhkarma@gmail.com"

Create a new repository

git clone https://gitlab.com/anirudhkarma/crud-operations.git
cd crud-operation
git switch --create main
touch README.md
git add README.md
git commit -m "add README"
git push --set-upstream origin main

Push an existing folder

cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/anirudhkarma/crud-operation.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main

Push an existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/anirudhkarma/crud-operation.git
git push --set-upstream origin --all
git push --set-upstream origin --tags