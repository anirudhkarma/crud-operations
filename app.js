// app.js
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const ejs = require('ejs');

const app = express();

// Set EJS as view engine
app.set('view engine', 'ejs');

// Body parser middleware
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// MongoDB connection
mongoose.connect('mongodb://localhost:27017/crud_app', { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('MongoDB connected'))
    .catch(err => console.log(err));

// Define Schema
const ItemSchema = new mongoose.Schema({
    name: String,
    age: Number,
    phoneNumber: Number,
    city: String
});

//Define a model
const Item = mongoose.model('Item',ItemSchema);


//Home route - display all documents
app.get('/', (req, res) => {
   Item.find({})
        .then(items => {
            res.render('index', { items: items });
        })
        .catch(err => {
            console.log(err);
        });
});

// Create route - form to create a new document
app.get('/create', (req,res)=> {
    res.render('create');
})

// Create POST route - handle form submission
app.post('/create', (req, res) => {
	const newItem = new Item({
        name: req.body.name,
        age: req.body.age,
        phoneNumber: req.body.phoneNumber,  
        city: req.body.city
    });

    newItem.save()
        .then(() => {
            res.redirect('/');
        })
        .catch(err => {
            console.log(err);
        });
});

// Update route - form to update a document
app.get('/update/:id', (req, res) => {
    Item.findById(req.params.id)
    .then(foundItem => {
            res.render('update', { item: foundItem });
        })
        .catch(err => {
            console.log(err);
        });
});

// Update post route - handle form submission for update
app.post('/update/:id', async (req, res) => {
    const { name, age, number, city } = req.body;
    try {
        await Item.findByIdAndUpdate(req.params.id, { name, age, number, city });
        res.redirect('/');
    } catch (err) {
        console.log(err);
        res.status(500).send('Error updating item');
    }
});

// Delete route
app.post('/delete/:id', (req, res) => {
    Item.findByIdAndDelete(req.params.id)
        .then(() => {
            res.redirect('/');
        })
        .catch(err => {
            console.log(err);
        });
});

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
